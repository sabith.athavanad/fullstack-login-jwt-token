const express = require("express");
const jwt = require("jsonwebtoken");
const { Client } = require("pg");
const cors = require("cors");
const app = express();

//var bodyParser = require('body-parser');
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//db connectivity configuration
const client = new Client({
  host: "localhost",
  port: 5432,
  user: "postgres",
  password: "sabith123",
  database: "test_db",
});

client.connect((err) => {
  if (err) throw err;
  console.log("connected to DB");
});

app.use(cors());

//login credentials checking API
app.post("/login", (req, res) => {
  const { username, password } = req.body;
  let qry = `select * from user1 where userName='${username}' and password='${password}'`;
  client.query(qry, (err, result) => {
    if (err || result.rows.length == 0) {
      console.log("Username or password is incorrect");
      res.status(200).send({ message: "Username or password is incorrect" });
    } else {
      //data to be encoded
      const resp = {
        id: result.rows[0].user_id,
        display_name: result.rows[0].fullname,
      };

      //no expiration time is defined on token signIn
      let token = jwt.sign({ user: resp }, "secretkey");
      res.status(200).send({
        auth: true,
        token: token,
        display_name: result.rows[0].fullname,
      });
    }
  });
});

//API to register the user
app.post("/register", (req, res) => {
  const { fullName, username, password } = req.body;
  let qr = `insert into user1(fullname,username,password) values('${fullName}','${username}','${password}')`;
  client.query(qr, (err, result) => {
    if (err) {
      console.log("User creation failed");
      res.status(404).send({ error: "Insertion failed" });
    } else {
      let txt = `${fullName} User created successfully`;
      res.send({ success: txt });
    }
  });
});

//api for verification, but not used here, verifyToken is used as middleware
app.get("/api/verify", verifyToken, (req, res) => {
  //res.status(200).send({ message: "Successfull" });

  let qr = `select * from user1`;
  client.query(qr, (err, result) => {
    if (err) {
      console.log("User details fetching failed");
      res.send({ error: "fetching failed" });
    } else {
      let txt = ` fetched successfully`;
      //console.log(result.rows);
      res.send( result.rows );
    }
  });
});

//moddleware for JWT token verification
function verifyToken(req, res, next) {
  const bearerHeader = req.headers["authorization"];
  console.log(typeof bearerHeader);

  if (typeof bearerHeader === undefined) {
    console.log("invalid token");
    res.status(500).send({ message: "No token provided" }); //unauthorized client
  }
  let token = bearerHeader.split(" ")[1];

  //spliting the token from Bearer <token>

  jwt.verify(token, "secretkey", (err, authData) => {
    if (err) {
      console.log("Authentication failed")
      res.status(401).send({ message: "Authentication failed" });

    } else {
      //res.send(authData);
      // req.response = authData;
      // console.log(req.response);
      console.log("Authentication success");
      next();
    }
  });
}

app.listen(3001, () => {
  console.log("Server started on 3001");
});
