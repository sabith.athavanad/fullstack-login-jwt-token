import React from "react";
import "./App.css";
import { BrowserRouter, Route } from "react-router-dom";
import Dashboard from "./components/Dashboard";
import SignUp from "./components/signUp/SignUp";
import Login from "./components/Login/Login";
import SecondComponent from "./components/SecondComponent";
import useToken from "./useToken";

function App() {
  const { setToken } = useToken();
  return (
    <>
      <BrowserRouter>
        <Route path="/" exact>
          <Dashboard />
        </Route>
        <Route path="/login">
          <Login setToken={setToken}/>
        </Route>
        <Route path="/signUp">
          <SignUp />
        </Route>
        <Route path="/secondComponent">
          <SecondComponent />
        </Route>
      </BrowserRouter>
    </>
  );
}

export default App;
