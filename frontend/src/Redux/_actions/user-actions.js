import { userServices } from "../_services/user-services";

export const userActions = {
  getAllUsers,
};

function getAllUsers() {
  return (dispatch) => {
    

    userServices.getAllUsers().then(
      (users) => dispatch(success(users)),
      (error) => dispatch(failure(error.message.toString()))
    );
  };

  
  function success(users) {
    return { type: "USERS_GETALL_SUCCESS", users };
  }
  function failure(error) {
    return { type: "USERS_GETALL_FAILURE", error };
  }
}
