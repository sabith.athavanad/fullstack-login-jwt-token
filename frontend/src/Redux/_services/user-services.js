import { authHeader } from "../_helper/authHeader";
import axios from "axios";

export const userServices = {
  getAllUsers,
};

async function getAllUsers() {
  const requestOptions = {
    headers: authHeader(),
  };
  
  try {
    const response = await axios.get(
      "http://localhost:3001/api/verify",
      requestOptions
    );
    return handleResponse(response);
  } catch (error) {
    return handleResponse(error.response);
  }
}

const handleResponse = (response) => {
  //console.log(response.data);

  if (response.status === 401) {
    localStorage.setItem("token", false); 

    return Promise.reject(response.data);
  }

  return response.data;
};
