const i = [{fullname:" "}]
export function users(state = {items:i}, action) {
  switch (action.type) {
    
    case 'USERS_GETALL_SUCCESS':
      return {
        items: action.users,
        loggedIn:true
      };
    case 'USERS_GETALL_FAILURE':
      return {
        error: action.error,
        loggedIn:false
      };
    default:
      return state;
  }
}
