import { useState } from "react";

const useToken = () => {

  //collects token from local storage
  const getToken = () => {
    const tokenString = localStorage.getItem("token");
    const userToken = JSON.parse(tokenString);
    return userToken;
  };

  //setting the state
  const [token, setToken] = useState(getToken());

  //saves the token to the local which is returned from server
  const saveToken = (userToken) => {
    localStorage.setItem("token", JSON.stringify(userToken));
    setToken(userToken);
  };

  
  return {
    setToken: saveToken,
    token,
  };
};

export default useToken;
