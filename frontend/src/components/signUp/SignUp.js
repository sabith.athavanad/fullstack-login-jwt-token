import React, { useState } from "react";
import "./signUp.css";
import { Link, useHistory } from "react-router-dom";

//post api for user Registration
async function signUpUser(credentials) {
  return fetch("http://localhost:3001/register", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((data) => data.json());
}

const SignUp = () => {
  //states for managing the input values
  const [fullName, setFullName] = useState("");
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");

  //for routing to another path
  let history = useHistory();

  //for handling the input values
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (fullName === "" || username === "" || password === "")
      alert("Fields cannot be blank");
    else {
      const response = await signUpUser({
        fullName,
        username,
        password,
      });
      //console.log(token);
      if (response.success) {
        alert(response.success);
        history.push("/login");
      } else alert(response.error);
    }
  };

  return (
    <div className="login">
      <form className="login__form" onSubmit={handleSubmit} autoComplete="off">
        <h1>SignUp Here</h1>
        <input
          type="text"
          placeholder="Enter Full Name"
          value={fullName}
          onChange={(e) => {
            setFullName(e.target.value);
          }}
        />
        <input
          type="text"
          placeholder="Enter the User Name"
          value={username}
          onChange={(e) => {
            setUserName(e.target.value);
          }}
        />

        <input
          type="password"
          placeholder="Enter the Password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        />

        <button type="submit" className="submit__btn">
          SignUp
        </button>
        <label className="label">
          <Link to="/login" style={{ textDecoration: "none" }}>
            If you have credentials...please Login
          </Link>
        </label>
      </form>
    </div>
  );
};

export default SignUp;
