import React from "react";
import {useSelector } from "react-redux";
import "./success.css"
const Success = () => {
    const users = useSelector((state) => state.users);
    console.log(users)
    
    
  return (
    <table className="content-table" border="1" cellPadding="2" cellSpacing="0">
      <tbody>
      <tr>
        <th>Full Name</th>
        <th>Username</th>
        <th>Password</th>
      </tr>
      
      
      {
          users.items.map((user,index) => (
            
            <tr key={index}>
            <td>{user.fullname}</td>
            <td>{user.username}</td>
            <td>{user.password}</td>
            
            </tr>
            
          ))}
          </tbody>
    </table>
  );
};

export default Success;
