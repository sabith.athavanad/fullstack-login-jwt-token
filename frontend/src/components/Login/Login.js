import React, { useState } from "react";
import "./Login.css";
import { Link, useHistory } from "react-router-dom";
import PropTypes from 'prop-types';

//getting the token,display_name from API
async function loginUser(credentials) {
  return fetch("http://localhost:3001/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((data) => data.json());
}


const Login = ({ setToken }) => {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");

  //for routing to another path
  let history = useHistory();

  //for handle the values from input
  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = await loginUser({
      username,
      password,
    });

    //message and setting token in local storage according to information from server
    token.message ? alert(token.message) : setToken(token);
    history.push("/");
  };

  return (
    <div className="login">
      <form className="login__form" onSubmit={handleSubmit} autoComplete="off">
        <h1>Login Here</h1>
        <input
          type="text"
          placeholder="Enter the user Name"
          value={username}
          onChange={(e) => {
            setUserName(e.target.value);
          }}
        />

        <input
          type="password"
          placeholder="Enter the Password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        />

        <button type="submit" className="submit__btn">
          Login
        </button>
        <label className="label">
          <Link to="/signUp" style={{ textDecoration: "none" }}>
            Not have an account ? Sign up here
          </Link>
        </label>
      </form>
    </div>
  );
};

Login.propTypes = {
  setToken: PropTypes.func.isRequired
}
export default Login;
