import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { userActions } from "../Redux/_actions/user-actions";
import useToken from "../useToken";
import Success from "./secondPage/success";
import Fail from "./secondPage/fail";

const SecondComponent = () => {
  const { token } = useToken();

  const users = useSelector((state) => state.users);
  // console.log(users)

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(userActions.getAllUsers());
  }, [token]);

  return <div>{users.loggedIn ? <Success /> : <Fail />}</div>;
};

export default SecondComponent;
