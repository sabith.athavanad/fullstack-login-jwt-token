import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {  useHistory } from "react-router-dom";
import "./Logout/logout.css";
import Login from "./Login/Login";
import useToken from "../useToken";
//import axios from "axios";
import { userActions } from "../Redux/_actions/user-actions";


const Dashboard = () => {
  //collects the existing token status from local storage
  const { token, setToken } = useToken();
  
  const users = useSelector((state) => state.users);
  console.log(users)
  const dispatch = useDispatch();

  let history = useHistory();

  useEffect(() => {
    if(token) dispatch(userActions.getAllUsers());
  },[token]);

  if (!token) {
    return <Login setToken={setToken} />;
  }

  //clearing the token from local storage
  const clearToken = () => {
    setToken(false);
  };

  const nextComponent = ()=>{
    history.push("/secondComponent")
  }

  return (
    <div>
      <h3>
        Welcome <span className="user__name">{token.display_name}</span>
      </h3>
      <button className="logout__button" onClick={clearToken}>
        Logout
      </button>
      <h3>Full Names of users</h3>
      <h5>
        <ul>
          
          
           
          {
          users.items.map((user,index) => (
            <li key={index}>{user.fullname}</li>
          ))}
          
        </ul>
      </h5>

      <button className="logout__button" onClick={nextComponent}>
        Goto next Component &gt;&gt;
      </button>
    </div>
  );
};

export default Dashboard;
